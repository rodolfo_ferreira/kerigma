# README #

This algorithm is an Compiler for the language "Kerigma", created by the professor
Pablo Azevedo to the discipline "Compilers".
This program uses the methods of a general compiler to create runnable programs to the JVM.
It has its own syntax, the project is divided in Lexemma, Parse and Syntatic analyse.

### What is this repository for? ###

* This repository was created as backup and version control

### How do I get set up? ###

* Import in your preffered IDE and run.

### Contribution guidelines ###

* Build and developer by Rodolfo Ferreira and Juliana Ferreira.

### Who do I talk to? ###

* For further info contact me through RodolfoAndreFerreira@gmail.com