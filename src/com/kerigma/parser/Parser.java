/*
 * 
 * Copyright (c) 2015, Rodolfo Ferreira and Juliana Ferreira. All rights reserved. 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is a part of the Kerigma compiler, created to learn how compilers works.
 * It was implemented to our degree in Computer Science, in the discipline 
 * called "compilers", given by professor Pablo Sampaio, from 
 * Federal Rural University from Pernambuco.
 *
 * Please contact us, rodolfoandreferreira@gmail.com or julifs.13@gmail.com
 * if you need additional information or have any questions.
 * 
 */
package com.kerigma.parser;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import com.kerigma.exceptions.ParserException;
import com.kerigma.exceptions.TokenException;
import com.kerigma.lexer.Lexer;
import com.kerigma.lexer.Token;
import com.kerigma.lexer.TokenType;

import dolphs.util.tree.Leaf;
import dolphs.util.tree.Node;
import dolphs.util.tree.Tree;

/**
 * This class is responsible to analise a sequence of tokens and determinate the
 * grammatical structure.
 * 
 * @author Rodolfo Ferreira and Juliana Ferreira.
 */
public class Parser {

	/** The tokenizer to receive the tokens. */
	private Lexer lexer;

	/** The current token from tokenizer. */
	private Token currentToken;

	/**
	 * This method instantiates a new parser, instantiating a new Tokenizer to
	 * analyze tokens.
	 */
	public Parser() {
		this.lexer = new Lexer();
	}

	/**
	 * This method resets the InputStream start to read the tokens and starts to
	 * parse the Program.
	 *
	 * @param input
	 *            the input stream, which contains the code
	 * @return Tree, the header node of the syntax tree
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public Tree parse(InputStream input) throws ParserException,
			TokenException, FileNotFoundException {
		lexer.reset(input);
		currentToken = lexer.nextToken();
		Tree tree = parseProgram(null);
		acceptToken(TokenType.EOF);
		return tree;
	}

	/**
	 * Method that accepts the Token checked before.
	 *
	 * @return the syntaxt tree of this node
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree acceptToken() throws TokenException {
		Leaf<Token> leaf = new Leaf<Token>(currentToken);
		currentToken = lexer.nextToken();
		return leaf;
	}

	/**
	 * Method responsible to parse a program.
	 * 
	 * <Program> ::= <Global Statement> <Global Statement>*
	 *
	 * @param node
	 *            the node
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseProgram(Node<FunctionType> node) throws ParserException,
			TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.PROGRAMA);
		node.addChildren(parseGlobalStmt());
		if (currentToken.getTokenType() != TokenType.EOF)
			parseProgram(node);
		return node;

	}

	/**
	 * Method that try to accepts the Token expected, if it is not the expected
	 * token will throw an exception.
	 *
	 * @param tokenType
	 *            the token type
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree acceptToken(TokenType tokenType) throws ParserException,
			TokenException {
		Leaf<Token> leaf = new Leaf<Token>(currentToken);
		if (currentToken.getTokenType() == tokenType) {
			currentToken = lexer.nextToken();

		} else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1 - 1, tokenType);
		}
		return leaf;

	}

	/**
	 * Method responsible to parse a global statement.
	 * 
	 * <Global Statement> ::= <Variable Statement>| <Function Statement>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseGlobalStmt() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(
				FunctionType.DECL_GLOBAL);
		if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			node.addChildren(parseVarStmt());
		} else if (currentToken.getTokenType() == TokenType.RES_FUNC) {
			node.addChildren(parseFuncStmt());
		} else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1,
					TokenType.IDENTIFIER, TokenType.RES_FUNC);
		}
		return node;
	}

	/**
	 * Method responsible to parse a variable statement.
	 * 
	 * <Variable Statement> ::= <Identyfiers List> ":" <Type List> ";"
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseVarStmt() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(
				FunctionType.DECL_VARIAVEL);
		node.addChildren(parseIdList(null));
		node.addChildren(acceptToken(TokenType.SP_COLON));
		node.addChildren(parseWordTypeList(null));
		node.addChildren(acceptToken(TokenType.SP_SEMICOLON));
		return node;
	}

	/**
	 * Method responsible to parse an identifiers list.
	 * 
	 * <Identifiers List> ::= IDENTIFIER ("," IDENTIFIER)*
	 *
	 * @param node
	 *            the node
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseIdList(Node<FunctionType> node) throws ParserException,
			TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.LISTA_IDENTS);
		if (currentToken.getTokenType() == TokenType.SP_COLON
				|| currentToken.getTokenType() == TokenType.OP_ASSIGNMENT)
			return node;
		node.addChildren(acceptToken(TokenType.IDENTIFIER));
		if (currentToken.getTokenType() == TokenType.SP_COMMA) {
			node.addChildren(acceptToken());
			parseIdList(node);
		}
		return node;
	}

	/**
	 * Method responsible to parse a type list.
	 * 
	 * <Type List> ::= <Type> ("," <Type>)*
	 *
	 * @param node
	 *            the node
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseWordTypeList(Node<FunctionType> node)
			throws ParserException, TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.LISTA_TIPOS);
		node.addChildren(parseWordType());
		if (currentToken.getTokenType() == TokenType.SP_COMMA) {
			node.addChildren(acceptToken(TokenType.SP_COMMA));
			parseWordTypeList(node);
		}
		return node;
	}

	/**
	 * Method responsible to parse a type.
	 * 
	 * <Type> ::= "int" | "char" | "float"
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseWordType() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.TIPO);
		if (currentToken.getTokenType() == TokenType.RES_INT
				|| currentToken.getTokenType() == TokenType.RES_CHAR
				|| currentToken.getTokenType() == TokenType.RES_FLOAT)
			node.addChildren(acceptToken());
		else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1, TokenType.RES_INT,
					TokenType.RES_CHAR, TokenType.RES_FLOAT);
		}
		return node;

	}

	/**
	 * Method responsible to parse a function statement.
	 * 
	 * <Function Statement> ::= <Signature> <Block>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseFuncStmt() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.DECL_FUNC);
		node.addChildren(parseSignature());
		node.addChildren(parseBlock());
		return node;
	}

	/**
	 * Method responsible to parse a signature.
	 * 
	 * <Signature> ::= "func" ID. "(" <Formal Parameters> ")" ":" <Type List> |
	 * "func" * ID "(" <Formal Parameters> ")" ":" "void"
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseSignature() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(
				FunctionType.ASSINATURA);
		node.addChildren(acceptToken(TokenType.RES_FUNC));
		if (currentToken.getTokenType() == TokenType.IDENTIFIER
				|| currentToken.getTokenType() == TokenType.RES_MAIN) {
			node.addChildren(acceptToken());
		} else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1,
					TokenType.IDENTIFIER, TokenType.RES_MAIN);
		}
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_LEFT));
		node.addChildren(parseFormalParams(null));
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_RIGHT));
		node.addChildren(acceptToken(TokenType.SP_COLON));
		if (currentToken.getTokenType() == TokenType.RES_INT
				|| currentToken.getTokenType() == TokenType.RES_CHAR
				|| currentToken.getTokenType() == TokenType.RES_FLOAT) {
			node.addChildren(parseWordTypeList(null));
		} else if (currentToken.getTokenType() == TokenType.RES_VOID) {
			node.addChildren(acceptToken());
		} else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1, TokenType.RES_INT,
					TokenType.RES_CHAR, TokenType.RES_FLOAT, TokenType.RES_VOID);
		}
		return node;
	}

	/**
	 * Method responsible to parse a Formal Parameters.
	 * 
	 * <Formal Parameters> ::= IDENTYFIER <TYPE> ( "," IDENTYFIER <TYPE> )* |
	 * VAZIO
	 *
	 * @param node
	 *            the node
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseFormalParams(Node<FunctionType> node)
			throws ParserException, TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.PARAM_FORMAIS);
		if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			node.addChildren(acceptToken());
			node.addChildren(parseWordType());
			if (currentToken.getTokenType() == TokenType.SP_COMMA) {
				node.addChildren(acceptToken());
				parseFormalParams(node);
			}
		}
		return node;

	}

	/**
	 * Method responsible to parse a block.
	 * 
	 * <Block> ::= "{" <Command List> "}"
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseBlock() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.BLOCO);
		node.addChildren(acceptToken(TokenType.SP_CURLY_BRACKET_LEFT));
		node.addChildren(parseCommandList(null));
		node.addChildren(acceptToken(TokenType.SP_CURLY_BRACKET_RIGHT));
		return node;

	}

	/**
	 * Method responsible to parse a command list.
	 * 
	 * <Command List> ::= (<Command>)*
	 *
	 * @param node
	 *            the node
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseCommandList(Node<FunctionType> node)
			throws ParserException, TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.LISTA_COMANDOS);
		node.addChildren(parseCommand());
		if (currentToken.getTokenType() != TokenType.SP_CURLY_BRACKET_RIGHT) {
			parseCommandList(node);
		}

		return node;

	}

	/**
	 * Method responsible to parse a command.
	 * 
	 * <Command> ::= <Variable Statement> | <Assignment> | <iteration> |
	 * <Decision> | <Print> | <Return> | <Block>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseCommand() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.COMANDO);
		// Se o tipo do token for Identificador, procura qual expressão
		// selecionar. <Variable Statement> | <Assignment>
		if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			node.addChildren(parseSelectId());
		}

		// Caso comece com "while", analisa a iteração. <iteration>
		else if (currentToken.getTokenType() == TokenType.RES_WHILE) {
			node.addChildren(parseIteration());
		}
		// Caso comece com "if", analisa a decisão. <Decision>
		else if (currentToken.getTokenType() == TokenType.RES_IF) {
			node.addChildren(parseDecision());
		}

		// Caso comece com "print", analisa a escrita. <Print>
		else if (currentToken.getTokenType() == TokenType.RES_PRINT) {
			node.addChildren(parsePrint());
		}

		// Caso comece com "return", analisa o retorno. <Return>
		else if (currentToken.getTokenType() == TokenType.RES_RETURN) {
			node.addChildren(parseReturn());
		}

		// Caso comece com "{", analisa o bloco. <Block>
		else if (currentToken.getTokenType() == TokenType.SP_CURLY_BRACKET_LEFT) {
			node.addChildren(parseBlock());
		}

		// Caso não tenha entrado em nenhum deles, o Token não é esperado.
		else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1,
					TokenType.IDENTIFIER, TokenType.RES_IF,
					TokenType.RES_PRINT, TokenType.RES_RETURN,
					TokenType.SP_CURLY_BRACKET_LEFT);
		}

		return node;
	}

	/**
	 * Method responsible to match a correspondent command to expressions
	 * started with identifiers.
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseSelectId() throws ParserException, TokenException {
		Node<FunctionType> unknown = new Node<FunctionType>(null);
		unknown.addChildren(acceptToken(TokenType.IDENTIFIER));
		// Procura se o próximo token é uma ',', caso seja analisa a lista e
		// processa o resto da expressão
		if (currentToken.getTokenType() == TokenType.SP_COMMA
				|| currentToken.getTokenType() == TokenType.SP_COLON
				|| currentToken.getTokenType() == TokenType.OP_ASSIGNMENT) {
			Node<FunctionType> node = unknown;
			unknown = new Node<FunctionType>(null);
			node.set(FunctionType.LISTA_IDENTS);
			if (currentToken.getTokenType() == TokenType.SP_COMMA)
				node.addChildren(acceptToken());
			unknown.addChildren(parseIdList(node));
			parseRestSelect(unknown);
		}

		// Se o próximo token for "(" é uma chamada de função. <Chamada de
		// Função>
		else if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT) {
			unknown.set(FunctionType.CHAMADA_FUNC);
			parseFuncCallCompl(unknown);
			unknown.addChildren(acceptToken(TokenType.SP_SEMICOLON));

		} else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1, TokenType.SP_COLON,
					TokenType.OP_ASSIGNMENT);
		}

		return unknown;

	}

	/**
	 * Parses the func call compl.
	 *
	 * @param node
	 *            the node
	 * @throws TokenException
	 *             the token exception
	 * @throws ParserException
	 *             the parser exception
	 */
	private void parseFuncCallCompl(Node<FunctionType> node)
			throws TokenException, ParserException {
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_LEFT));
		node.addChildren(parseExprList(null));
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_RIGHT));

	}

	/**
	 * Parses the rest select.
	 *
	 * @param unknown
	 *            the unknown
	 * @throws TokenException
	 *             the token exception
	 * @throws ParserException
	 *             the parser exception
	 */
	private void parseRestSelect(Node<FunctionType> unknown)
			throws TokenException, ParserException {
		// Se o próximo token for ":" é uma declaração de variável.
		// <Variable
		// Statement>.
		if (currentToken.getTokenType() == TokenType.SP_COLON) {
			unknown.set(FunctionType.DECL_VARIAVEL);
			unknown.addChildren(acceptToken());
			unknown.addChildren(parseWordTypeList(null));
			unknown.addChildren(acceptToken(TokenType.SP_SEMICOLON));
		}

		// Se o próximo token for "=" é uma atribuição. <Assignment>
		else if (currentToken.getTokenType() == TokenType.OP_ASSIGNMENT) {
			unknown.set(FunctionType.ATRIBUICAO);
			unknown.addChildren(acceptToken());
			unknown.addChildren(parseExpr());
			unknown.addChildren(acceptToken(TokenType.SP_SEMICOLON));
		}

		else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1, TokenType.SP_COLON,
					TokenType.OP_ASSIGNMENT);
		}

	}

	/**
	 * Method responsible to parse a iteration.
	 * 
	 * <iteration> ::= "while" "(" <Expression> ")" <Command>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseIteration() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.ITERACAO);
		node.addChildren(acceptToken(TokenType.RES_WHILE));
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_LEFT));
		node.addChildren(parseExpr());
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_RIGHT));
		node.addChildren(parseCommand());
		return node;

	}

	/**
	 * Method responsible to parse a decision.
	 * 
	 * <iteration> ::= ::= "if" "(" <Expression> ")" <Command> "else" <Command>
	 * | "if" "(" <Expression> ")" <Command>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseDecision() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.DECISAO);
		node.addChildren(acceptToken(TokenType.RES_IF));
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_LEFT));
		node.addChildren(parseExpr());
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_RIGHT));
		node.addChildren(parseCommand());
		if (currentToken.getTokenType() == TokenType.RES_ELSE) {
			node.addChildren(acceptToken());
			node.addChildren(parseCommand());
		}
		return node;

	}

	/**
	 * Method responsible to parse a print.
	 * 
	 * <Print> ::= "print" "(" <Expression> ")" ";"
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parsePrint() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.ESCRITA);
		node.addChildren(acceptToken(TokenType.RES_PRINT));
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_LEFT));
		node.addChildren(parseExpr());
		node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_RIGHT));
		node.addChildren(acceptToken(TokenType.SP_SEMICOLON));
		return node;
	}

	/**
	 * Method responsible to parse a return.
	 *
	 * <Return> ::= "return" <ExprList> ";"
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseReturn() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.RETORNO);
		node.addChildren(acceptToken(TokenType.RES_RETURN));
		node.addChildren(parseExprList(null));
		node.addChildren(acceptToken(TokenType.SP_SEMICOLON));
		return node;

	}

	/**
	 * Method responsible to parse the expression list.
	 * 
	 * <ExprList> ::= VAZIO | <Expression> ("," <Expression>)*
	 *
	 * @param node
	 *            the node
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseExprList(Node<FunctionType> node) throws ParserException,
			TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.LISTA_EXPRS);
		if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT
				|| currentToken.getTokenType() == TokenType.OP_NEGATION
				|| currentToken.getTokenType() == TokenType.OP_SUBTRACTION
				|| currentToken.getTokenType() == TokenType.CHAR
				|| currentToken.getTokenType() == TokenType.FLOAT
				|| currentToken.getTokenType() == TokenType.NUMBER
				|| currentToken.getTokenType() == TokenType.IDENTIFIER) {
			node.addChildren(parseExpr());
			if (currentToken.getTokenType() == TokenType.SP_COMMA) {
				node.addChildren(acceptToken());
				parseExprList(node);
			}
		}
		return node;
	}

	/**
	 * Method responsible to begin to parse an expression.
	 * 
	 * <Expression> ::= <ExprA>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseExpr() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(FunctionType.EXPRESSAO);
		node.addChildren(parseExprA(null));
		return node;
	}

	/**
	 * Method responsible to parse the first level of an expression.
	 * 
	 * <ExprA> ::= <ExprB> <ExprAComp>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseExprA(Node<FunctionType> node) throws ParserException,
			TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.EXPRESSAO_A);
		node.addChildren(parseExprB(null));
		if (currentToken.getTokenType() == TokenType.OP_OR
				|| currentToken.getTokenType() == TokenType.OP_AND) {
			node.addChildren(acceptToken());
			parseExprA(node);
			normalizeNode(node, FunctionType.EXPRESSAO_A);
		}
		return node;

	}

	/**
	 * Method responsible to parse the complement of the first level of an
	 * expression.
	 * 
	 * <ExprAComp> ::= "&&" <ExprB> <ExprAComp> | "||" <ExprB> <ExprAComp>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	// private Tree parseExprAComp(Node<FunctionType> node)
	// throws ParserException, TokenException {
	//
	// return node;
	// }

/**
	 * Method responsible to parse the second level of an expression.
	 * 
	 * <ExprB> ::= "==" <ExprC> 
	 * 			| "!=" <ExprC> 
	 * 			| ">" <ExprC> 
	 * 			| "=>" <ExprC> 
	 * 			| "<" <ExprC> 
	 * 			| "=<" <ExprC>
	 * 			| <ExprC>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseExprB(Node<FunctionType> node) throws ParserException,
			TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.EXPRESSAO_B);
		node.addChildren(parseExprC(null));
		if (currentToken.getTokenType() == TokenType.REL_EQUALS
				|| currentToken.getTokenType() == TokenType.REL_DIFFERENT
				|| currentToken.getTokenType() == TokenType.REL_GREATER_THAN
				|| currentToken.getTokenType() == TokenType.REL_GREATER_OR_EQUALS
				|| currentToken.getTokenType() == TokenType.REL_LESS_THAN
				|| currentToken.getTokenType() == TokenType.REL_LESS_OR_EQUALS) {
			node.addChildren(acceptToken());
			parseExprB(node);
			normalizeNode(node, FunctionType.EXPRESSAO_B);

		}
		return node;
	}

	/**
	 * Method responsible to parse the third level of an expression.
	 * 
	 * <ExprC> ::= "+" <ExprD> | "-" <ExprD> | <ExprD>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseExprC(Node<FunctionType> node) throws ParserException,
			TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.EXPRESSAO_C);
		node.addChildren(parseExprD(null));
		if (currentToken.getTokenType() == TokenType.OP_SUM
				|| currentToken.getTokenType() == TokenType.OP_SUBTRACTION) {
			node.addChildren(acceptToken());
			parseExprC(node);
			normalizeNode(node, FunctionType.EXPRESSAO_C);
		}
		return node;
	}

	private void normalizeNode(Node<FunctionType> node,
			FunctionType functionType) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		while (length > 3) {
			Node<FunctionType> newNode = new Node<FunctionType>(functionType);
			newNode.addChildren(childrens.get(0));
			childrens.remove(0);
			newNode.addChildren(childrens.get(0));
			childrens.remove(0);
			newNode.addChildren(childrens.get(0));
			childrens.remove(0);
			Tree tree = childrens.get(0);
			childrens.remove(0);
			childrens.add(newNode);
			childrens.add(tree);
			length = childrens.size();
		}

		Tree leaf = null;
		for (int i = 0; i < childrens.size(); i++) {
			Tree tree = childrens.get(i);
			if (tree instanceof Leaf<?>) {
				leaf = tree;
				childrens.remove(i);
			}
		}
		childrens.add(childrens.get(0));
		childrens.remove(0);
		childrens.add(leaf);
		childrens.add(childrens.get(0));
		childrens.remove(0);

	}

	/**
	 * Method responsible to parse the fourth level of an expression.
	 *
	 * <ExprD> ::= "*" <BasicExpr> | "/" <BasicExpr> | "%" <BasicExpr> |
	 * <BasicExpr>
	 * 
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseExprD(Node<FunctionType> node) throws ParserException,
			TokenException {
		if (node == null)
			node = new Node<FunctionType>(FunctionType.EXPRESSAO_D);
		node.addChildren(parseBasicExpr());
		if (currentToken.getTokenType() == TokenType.OP_MULTIPLICATION
				|| currentToken.getTokenType() == TokenType.OP_DIVISION
				|| currentToken.getTokenType() == TokenType.OP_PERCENTAGE) {
			node.addChildren(acceptToken());
			parseExprD(node);
			normalizeNode(node, FunctionType.EXPRESSAO_D);
		}
		return node;
	}

	/**
	 * Method responsible to parse an basic expression.
	 *
	 * <BasicExpr> ::= "(" <Expr> ")" | INT | FLOAT | CHAR | IDENTIFICADOR | "-"
	 * <BasicExpr> | "!" <BasicExpr>
	 *
	 * @return the syntaxt tree of this node
	 * @throws ParserException
	 *             if some token not expected is read, it will be thrown by this
	 *             exception
	 * @throws TokenException
	 *             if some token is not recognized
	 */
	private Tree parseBasicExpr() throws ParserException, TokenException {
		Node<FunctionType> node = new Node<FunctionType>(
				FunctionType.EXPRESSAO_BASICA);
		if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT) {
			node.addChildren(acceptToken());
			node.addChildren(parseExpr());
			node.addChildren(acceptToken(TokenType.SP_PARENTHESIS_RIGHT));
		} else if (currentToken.getTokenType() == TokenType.NUMBER) {
			node.addChildren(acceptToken());
		} else if (currentToken.getTokenType() == TokenType.FLOAT) {
			node.addChildren(acceptToken());
		} else if (currentToken.getTokenType() == TokenType.CHAR) {
			node.addChildren(acceptToken());
		} else if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			node.addChildren(acceptToken());
			if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT) {
				parseFuncCallCompl(node);
			}
		} else if (currentToken.getTokenType() == TokenType.OP_NEGATION
				|| currentToken.getTokenType() == TokenType.OP_SUBTRACTION) {
			node.addChildren(acceptToken());
			node.addChildren(parseBasicExpr());
		} else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex(), TokenType.NUMBER,
					TokenType.FLOAT, TokenType.CHAR, TokenType.IDENTIFIER);
		}

		return node;

	}
}
