package com.kerigma.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.kerigma.parser.Parser;
import com.kerigma.semantic.SemanticAnalysis;

import dolphs.util.Analysis;
import dolphs.util.tree.GraphVizVisit;
import dolphs.util.tree.Tree;
import dolphs.util.tree.graphiz.GraphViz;

public class ParserTest {

	private static final String arquivoEntrada = "C:\\Users\\Rodolfo\\Documents\\teste3.txt";

	private static final String arquivoArvoreSaida = "C:\\Users\\Rodolfo\\Documents\\teste3";

	/**
	 * Este metodo executa um pequeno teste de reconhecimento.
	 */
	public static void main(String[] args) throws Exception {
		Parser parser = new Parser();
		InputStream entrada;

		System.out.println(" == TESTE DO PARSER ==\n");

		// TESTA O PARSER USANDO UMA STRING:

		// String codigo = "x, y, z : char, int, float; 1+3;while()1+3;x=3;";
		// System.out
		// .println(" Texto fonte a ser reconhecido: \"" + codigo + "\"");

		entrada = new FileInputStream(arquivoEntrada);

		// PARA TESTAR USANDO ARQUIVO:
		// entrada = new FileInputStream("caminho do arquivo");

		long startTime = System.nanoTime();
		Tree tree = parser.parse(entrada);
		long endTime = System.nanoTime();
		Analysis analyse = new Analysis(startTime, endTime);
		System.out.printf("Parse complete in %s without errors\n",
				analyse.toString());
		GraphVizVisit graph = new GraphVizVisit();
		tree.acceptVisitor(graph);
		try {
			File file = new File(arquivoArvoreSaida);
			startTime = System.nanoTime();
			graph.saveToFile(GraphViz.WINDOWS_OS, file, "pdf");
			endTime = System.nanoTime();
			analyse = new Analysis(startTime, endTime);
			System.out.printf("Tree image saved in %s in %s\n",
					file.getAbsolutePath(), analyse.toString());
		} catch (Exception e) {
			System.err.println("Tree image couldn't be saved");
		}

		SemanticAnalysis semantic = new SemanticAnalysis();
		startTime = System.nanoTime();
		tree.acceptVisitor(semantic);
		semantic.validate();
		endTime = System.nanoTime();
		analyse = new Analysis(startTime, endTime);
		System.out.printf(
				"Semantic analysis complete in %s without errors\n\n",
				analyse.toString());

		System.out.println(" == FIM ==");
	}

}
