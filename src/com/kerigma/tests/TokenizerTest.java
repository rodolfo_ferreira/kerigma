package com.kerigma.tests;

import com.kerigma.exceptions.TokenException;
import com.kerigma.lexer.Lexer;
import com.kerigma.lexer.Token;
import com.kerigma.lexer.TokenType;

public class TokenizerTest {
	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws TokenException
	 *             the token exception
	 */
	public static void main(String[] args) throws TokenException {
		Lexer lexer = new Lexer();
		Token token = null;

		System.out.println("\n\n\n");
		System.out.println(" == TESTE DO LEXER ==\n");
		System.out
				.println(" Digite alguma string terminada em \";\" e tecle ENTER:\n\n");
		System.out.print(" ");

		// passa a entrada padr�o para o lexer
		lexer.reset(System.in);

		// parar passar um arquivo como entrada
		// lexer.reset(new FileInputStream("caminho do arquivo"));

		do {
			token = lexer.nextToken();
			System.out.println("\t" + token);

		} while (token.getTokenType() != TokenType.EOF);

		System.out.println("\n == FIM ==");
	}

}
