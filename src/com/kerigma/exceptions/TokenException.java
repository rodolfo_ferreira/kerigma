/*
 * 
 * Copyright (c) 2015, Rodolfo Ferreira and Juliana Ferreira. All rights reserved. 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is a part of the Kerigma compiler, created to learn how compilers works.
 * It was implemented to our degree in Computer Science, in the discipline 
 * called "compilers", given by professor Pablo Sampaio, from 
 * Federal Rural University from Pernambuco.
 *
 * Please contact us, rodolfoandreferreira@gmail.com or julifs.13@gmail.com
 * if you need additional information or have any questions.
 * 
 */
package com.kerigma.exceptions;

/**
 * This class is an Exception subclass, it's used to throw exceptions of the
 * Tokenizer class
 * 
 * @author Rodolfo Ferreira and Juliana Ferreira.
 */
@SuppressWarnings("serial")
public class TokenException extends Exception {

	/** The Constant String used when is expected a number. */
	public static final String EXPECTED_NUMBER = "\"Expected only numbers after the first occurrence of '.'.\"";

	/** The Constant String used when is expected one operator '&'. */
	public static final String EXPECTED_AND = "\"Expected '&' to complete the logical operator.\"";

	/** The Constant String used when is expected one operator '''. */
	public static final String EXPECTED_APOSTROPHE = "\"Expected ''' to complete char.\"";

	/** The Constant String used when is expected one operator command. */
	public static final String EXPECTED_COMMAND = "\"Expected 'n', 'r', or 't' to complete char command.\"";

	/** The Constant String used when is expected one operator '|'. */
	public static final String EXPECTED_OR = "\"Expected '|' to complete the logical operator.\"";

	/** The Constant String used when is not expected the character '.'. */
	public static final String IDENTIFIER_DOT = "\"The character '.' is not expected in identifiers.\"";

	/** The Constant String used when is expected the end of the comment block. */
	public static final String COMMENT_END = "Comment end not found";

	/** The Constant String format to be used as error message */
	private static final String EXCEPTION_MESSAGE_FORMAT = "\n\tInvalid Token: '%c' at line %d and column %d.";

	/**
	 * Instantiates a new token exception only with message, it's only used
	 * to IOException cases.
	 *
	 * @param msg
	 *            the message to be shown
	 */
	public TokenException(String msg) {
		super(msg);

	}

	/**
	 * Instantiates a new token exception with the lexeme, line and column where
	 * the token was not recognized.
	 *
	 * @param lexeme
	 *            the lexeme which the problem was found
	 * @param line
	 *            the line where the lexeme is.
	 * @param column
	 *            the column where the lexeme is.
	 */
	public TokenException(char lexeme, int line, int column) {
		super(String.format(EXCEPTION_MESSAGE_FORMAT, lexeme, line, column));

	}

	/**
	 * Instantiates a new token exception with the lexeme, line and column where
	 * the token was not recognized, and a message with wich lexeme is expected.
	 *
	 * @param lexeme
	 *            the lexeme which the problem was found
	 * @param line
	 *            the line where the lexeme is.
	 * @param column
	 *            the column where the lexeme is.
	 * @param expected
	 *            the expected String containing the message with the expected
	 *            token
	 */
	public TokenException(char lexeme, int line, int column, String expected) {
		super(String.format(EXCEPTION_MESSAGE_FORMAT, lexeme, line, column)
				+ expected);

	}
}
