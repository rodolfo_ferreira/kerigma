/*
 * 
 * Copyright (c) 2015, Rodolfo Ferreira and Juliana Ferreira. All rights reserved. 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is a part of the Kerigma compiler, created to learn how compilers works.
 * It was implemented to our degree in Computer Science, in the discipline 
 * called "compilers", given by professor Pablo Sampaio, from 
 * Federal Rural University from Pernambuco.
 *
 * Please contact us, rodolfoandreferreira@gmail.com or julifs.13@gmail.com
 * if you need additional information or have any questions.
 * 
 */
package com.kerigma.jvm;

import java.io.FileNotFoundException;
import java.io.InputStream;

import com.kerigma.exceptions.ParserException;
import com.kerigma.exceptions.TokenException;
import com.kerigma.lexer.Lexer;
import com.kerigma.lexer.Token;
import com.kerigma.lexer.TokenType;
import com.kerigma.parser.FunctionType;

import dolphs.util.tree.Node;

/**
 * This class is responsible to analise a sequence of tokens and determinate the
 * grammatical structure.
 * 
 * @author Rodolfo Ferreira and Juliana Ferreira.
 */
public class JVM {

	/** The tokenizer to receive the tokens. */
	private Lexer lexer;

	/** The current token from tokenizer. */
	private Token currentToken;

	private String s;
	private String nomeArquivo;
	private int contadorLocals;
	private int contadorStack;

	public JVM(InputStream input, String nomeArquivo) throws ParserException,
			TokenException, FileNotFoundException {
		lexer.reset(input);
		currentToken = lexer.nextToken();
		this.nomeArquivo = nomeArquivo;
	}

	private void jvmProgram() throws ParserException,
			TokenException {
		s += ".class public " + nomeArquivo + "\n";
		s += ".super java/lang/Object\n";
		s += ".method public <init>()V \n" + "aload_0 \n";
		s += "invokenonvirtual java/lang/Object/<init>()V \n";
		s += "return\n";
		s += ".end method \n";
		jvmGlobalStmt();
		if (currentToken.getTokenType() != TokenType.EOF)
			jvmProgram();

	}

	private void jvmGlobalStmt() throws ParserException, TokenException {
		if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			jvmVarStmt();
		} else if (currentToken.getTokenType() == TokenType.RES_FUNC) {
			jvmFuncStmt();
		}
	}

	private void jvmVarStmt() throws ParserException, TokenException {
		jvmIdList();
		jvmWordTypeList();
	}

	private void jvmIdList() throws ParserException,
			TokenException {
		if (currentToken.getTokenType() == TokenType.SP_COLON
				|| currentToken.getTokenType() == TokenType.OP_ASSIGNMENT)
			
		if (currentToken.getTokenType() == TokenType.SP_COMMA) {
			jvmIdList();
		}
	}

	private void jvmWordTypeList()
			throws ParserException, TokenException {
		jvmWordType();
		if (currentToken.getTokenType() == TokenType.SP_COMMA) {
			jvmWordTypeList();
		}
	}

	private void jvmWordType() throws ParserException, TokenException {

		jvmWordType();
		if (currentToken.getTokenType() == TokenType.SP_COMMA) {
			jvmWordTypeList();
		}

	}

	private void jvmFuncStmt() throws ParserException, TokenException {
		jvmSignature();
		jvmBlock();
	}

	private void jvmSignature() throws ParserException, TokenException {
		s += ".method ";
		s += currentToken.toString();
		s += "()V \n";
		s += ".limit locals 10 \n.limit stack 10 \n"; // TODO n�o sei como
														// definir essas
														// quantidades
		jvmFormalParams();
		if (currentToken.getTokenType() == TokenType.RES_INT
				|| currentToken.getTokenType() == TokenType.RES_CHAR
				|| currentToken.getTokenType() == TokenType.RES_FLOAT) {
			jvmWordTypeList();
		}

	}

	private void jvmFormalParams()
			throws ParserException, TokenException {
		if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			jvmWordType();
			if (currentToken.getTokenType() == TokenType.SP_COMMA) {
				jvmFormalParams();
			}
		}
	}

	private void jvmBlock() throws ParserException, TokenException {
		jvmCommandList();
		s += "\n .end method";
	}

	private void jvmCommandList()
			throws ParserException, TokenException {
		jvmCommand();
		if (currentToken.getTokenType() != TokenType.SP_CURLY_BRACKET_RIGHT) {
			jvmCommandList();
		}

	}

	private void jvmCommand() throws ParserException, TokenException {
		if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			jvmSelectId();
		}

		else if (currentToken.getTokenType() == TokenType.RES_WHILE) {
			jvmIteration();
		}
		
		else if (currentToken.getTokenType() == TokenType.RES_IF) {
			jvmDecision();
		}
		
		else if (currentToken.getTokenType() == TokenType.RES_PRINT) {
			jvmPrint();
		}

		else if (currentToken.getTokenType() == TokenType.RES_RETURN) {
			jvmReturn();
		}

		else if (currentToken.getTokenType() == TokenType.SP_CURLY_BRACKET_LEFT) {
			jvmBlock();
		}

		else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1,
					TokenType.IDENTIFIER, TokenType.RES_IF,
					TokenType.RES_PRINT, TokenType.RES_RETURN,
					TokenType.SP_CURLY_BRACKET_LEFT);
		}
	}

	private void jvmSelectId() throws ParserException, TokenException {
		Node<FunctionType> unknown = new Node<FunctionType>();
		if (currentToken.getTokenType() == TokenType.SP_COMMA
				|| currentToken.getTokenType() == TokenType.SP_COLON
				|| currentToken.getTokenType() == TokenType.OP_ASSIGNMENT) {
			
			if (currentToken.getTokenType() == TokenType.SP_COMMA)
				
			jvmIdList();
			jvmRestSelect(unknown);
		}

		else if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT) {
			jvmFuncCallCompl();

		} else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1, TokenType.SP_COLON,
					TokenType.OP_ASSIGNMENT);
		}


	}

	private void jvmFuncCallCompl()
			throws TokenException, ParserException {
		jvmExprList();
	}

	private void jvmRestSelect(Node<FunctionType> unknown)
			throws TokenException, ParserException {
		if (currentToken.getTokenType() == TokenType.SP_COLON) {
			jvmWordTypeList();
		}

		else if (currentToken.getTokenType() == TokenType.OP_ASSIGNMENT) {
			s += "\n bipush ";
			jvmExpr();
			s += "\n istore ";
			contadorLocals++;
			s += "\n istore ";
			s += Integer.toString(contadorLocals, 0);
			contadorStack++;
			s += "\n iload ";
			s += Integer.toString(contadorStack, 0);
		}

		else {
			throw new ParserException(currentToken.getTokenType(),
					lexer.getLine(), lexer.getIndex() - 1, TokenType.SP_COLON,
					TokenType.OP_ASSIGNMENT);
		}

	}

	private void jvmIteration() throws ParserException, TokenException {
		// TODO while aqui
		jvmExpr();
		jvmCommand();

	}

	private void jvmDecision() throws ParserException, TokenException {
		s += "\n if_";
		jvmExpr();
		s += "parteElse";
		jvmCommand();
		if (currentToken.getTokenType() == TokenType.RES_ELSE) {
			s += "\n\nparteElse:";
			jvmCommand();
		}

	}

	private void jvmPrint() throws ParserException, TokenException {
		s += "\n getstatic java/lang/System/out Ljava/io/PrintStream;";
		s += "\n ldc \"";
		jvmExpr();
		s += "\"";
		s += "\n invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V";
	}

	private void jvmReturn() throws ParserException, TokenException {
		s += "\n ireturn";
		jvmExprList();

	}

	private void jvmExprList() throws ParserException,
			TokenException {
		if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT
				|| currentToken.getTokenType() == TokenType.OP_NEGATION
				|| currentToken.getTokenType() == TokenType.OP_SUBTRACTION
				|| currentToken.getTokenType() == TokenType.CHAR
				|| currentToken.getTokenType() == TokenType.FLOAT
				|| currentToken.getTokenType() == TokenType.NUMBER
				|| currentToken.getTokenType() == TokenType.IDENTIFIER) {
			jvmExpr();
			if (currentToken.getTokenType() == TokenType.SP_COMMA) {
				jvmExprList();
			}
		}
	}

	private void jvmExpr() throws ParserException, TokenException {
		jvmExprA();
	}

	private void jvmExprA() throws ParserException, TokenException {
		jvmExprB();
		jvmExprAComp();
	}

	private void jvmExprAComp() throws ParserException, TokenException {
		// TODO n�o achei pra or e and
		if (currentToken.getTokenType() == TokenType.OP_OR) {
			s += "";
		}else if (currentToken.getTokenType() == TokenType.OP_AND) {
			s += "";
		}
		jvmExprB();
		jvmExprAComp();
	}

	private void jvmExprB() throws ParserException, TokenException {
		jvmExprC();
		if (currentToken.getTokenType() == TokenType.REL_EQUALS) {
			s += "icmpeq";
		} else if (currentToken.getTokenType() == TokenType.REL_DIFFERENT) {
			s += "icmpg";
		} else if (currentToken.getTokenType() == TokenType.REL_GREATER_THAN) {
			s += "icmple";
		} else if (currentToken.getTokenType() == TokenType.REL_GREATER_OR_EQUALS) {
			s += "";// TODO n�o achei
		} else if (currentToken.getTokenType() == TokenType.REL_LESS_THAN) {
			s += "icmplt";
		} else if (currentToken.getTokenType() == TokenType.REL_LESS_OR_EQUALS) {
			s += "";// TODO n�o achei
		}
		jvmExprB();
	}

	private void jvmExprC() throws ParserException, TokenException {
		jvmExprD();
		if (currentToken.getTokenType() == TokenType.OP_SUM) {
			s += "\n iinc";
		} else if (currentToken.getTokenType() == TokenType.OP_SUM) {
			s += "\n iadd";
		}
		jvmExprC();
	}

	private void jvmExprD() throws ParserException, TokenException {
		jvmBasicExpr();
		if (currentToken.getTokenType() == TokenType.OP_MULTIPLICATION) {
			s += "\n imul";
		} else if (currentToken.getTokenType() == TokenType.OP_DIVISION) {
			s += "\n idiv";
		} else if (currentToken.getTokenType() == TokenType.OP_PERCENTAGE) {
			s += "\n ";// TODO n�o achei
		}
		jvmExprD();
	}

	private void jvmBasicExpr() throws ParserException, TokenException {
		if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT) {

		} else if (currentToken.getTokenType() == TokenType.NUMBER) {
			s += currentToken.toString();
		} else if (currentToken.getTokenType() == TokenType.FLOAT) {
			s += currentToken.toString();
		} else if (currentToken.getTokenType() == TokenType.CHAR) {
			s += currentToken.toString();
		} else if (currentToken.getTokenType() == TokenType.IDENTIFIER) {
			if (currentToken.getTokenType() == TokenType.SP_PARENTHESIS_LEFT) {
				jvmFuncCallCompl();
			}
		} else if (currentToken.getTokenType() == TokenType.OP_NEGATION
				|| currentToken.getTokenType() == TokenType.OP_SUBTRACTION) {
			jvmBasicExpr();
		}

	}

	public String getS() {
		return s;
	}
}