/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */package com.kerigma.lexer;

// TODO: Auto-generated Javadoc
/**
 * This Enum represents the types of tokens of the Kerigma language.
 * 
 * @author Rodolfo Ferreira
 * 
 */
public enum TokenType {

	/** The literal number representation([0-9]+). */
	NUMBER,

	/** The literal float representation([0-9]+.[0-9]+). */
	FLOAT,

	/** The literal char representation('[A-z]{1}'). */
	CHAR,

	/** The identifier representation([A-z]+[0-9]*). */
	IDENTIFIER,

	/** The end of file representation(EOF). */
	EOF,

	/** The relationship "greater than" representation(>). */
	REL_GREATER_THAN,

	/** The relationship "less than" representation(<). */
	REL_LESS_THAN,

	/** The relationship "greater or equals" representation(>=). */
	REL_GREATER_OR_EQUALS,

	/** The relationship "less or equals" representation(<=). */
	REL_LESS_OR_EQUALS,

	/** The relationship "equals" representation(==). */
	REL_EQUALS,

	/** The relationship "different" representation(!=). */
	REL_DIFFERENT,

	/** The operator of "sum" representation(+). */
	OP_SUM,

	/** The operator of "subtraction" representation(-). */
	OP_SUBTRACTION,

	/** The operator of "multiplication" representation(*). */
	OP_MULTIPLICATION,

	/** The operator of "division" representation(/). */
	OP_DIVISION,

	/** The operator of "percentage" representation(%). */
	OP_PERCENTAGE,

	/** The operator of "and" representation(&&). */
	OP_AND,

	/** The operator of "or" representation(||). */
	OP_OR,

	/** The operator of negation representation(!). */
	OP_NEGATION,

	/** The operator of assignment representation(=). */
	OP_ASSIGNMENT,

	/** The special character "parenthesis left" representation((). */
	SP_PARENTHESIS_LEFT,

	/** The special character "parenthesis right" representation()). */
	SP_PARENTHESIS_RIGHT,

	/** The special character "comma" representation(,). */
	SP_COMMA,

	/** The special character "semicolon" representation(;). */
	SP_SEMICOLON,

	/** The special character "colon" representation(:). */
	SP_COLON,

	/** The special character "curly bracket left" representation({). */
	SP_CURLY_BRACKET_LEFT,

	/** The special character "curly bracket right" representation(}). */
	SP_CURLY_BRACKET_RIGHT,

	/** The reserved word "main" representation("main"). */
	RES_MAIN,

	/** The reserved word "if" representation("if"). */
	RES_IF,

	/** The reserved word "else" representation("else"). */
	RES_ELSE,

	/** The reserved word "while" representation("while"). */
	RES_WHILE,

	/** The reserved word "int" representation("int"). */
	RES_INT,

	/** The reserved word "float" representation("float"). */
	RES_FLOAT,

	/** The reserved word "char" representation("char"). */
	RES_CHAR,

	/** The reserved word "void" representation("void"). */
	RES_VOID,

	/** The reserved word "func" representation("func"). */
	RES_FUNC,

	/** The reserved word "print" representation("print"). */
	RES_PRINT,

	/** The reserved word "return" representation("return"). */
	RES_RETURN

}
