/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package com.kerigma.lexer;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import com.kerigma.exceptions.TokenException;

// TODO: Auto-generated Javadoc
/**
 * This class is responsible to lexical analysis, which consists in a process of
 * converting a sequence of characters into a sequence of tokens.
 * 
 * @author Rodolfo Ferreira and Juliana Ferreira.
 * 
 */
public class Lexer {

	/** The input stream which contains the content to be generated as token. */
	private InputStream inputStream;

	/** The next char to be analysed as token. */
	private int nextChar;

	/** The index of the string line which is being processed. */
	private int index;

	/** The line of the code which is being processed. */
	private int line;

	/** The hashmap for the reserverd words. */
	private static HashMap<String, TokenType> RES_WORDS;

	/**
	 * Instantiates a new lexer.
	 */
	public Lexer() {
		RES_WORDS = new HashMap<String, TokenType>();
		RES_WORDS.put("main", TokenType.RES_MAIN);
		RES_WORDS.put("if", TokenType.RES_IF);
		RES_WORDS.put("else", TokenType.RES_ELSE);
		RES_WORDS.put("while", TokenType.RES_WHILE);
		RES_WORDS.put("int", TokenType.RES_INT);
		RES_WORDS.put("float", TokenType.RES_FLOAT);
		RES_WORDS.put("char", TokenType.RES_CHAR);
		RES_WORDS.put("void", TokenType.RES_VOID);
		RES_WORDS.put("func", TokenType.RES_FUNC);
		RES_WORDS.put("print", TokenType.RES_PRINT);
		RES_WORDS.put("while", TokenType.RES_WHILE);
		RES_WORDS.put("return", TokenType.RES_RETURN);
	}

	/**
	 * This method resets the inputstream to prepare the analysis.
	 *
	 * @param inputStream
	 *            the input stream to be analyzed
	 * @throws TokenException
	 *             the token exception if some IOException is caught.
	 */
	public void reset(InputStream inputStream) throws TokenException {
		line = 1;
		index = 1;
		try {
			// cont�m o codigo fonte a ser analisado
			this.inputStream = inputStream;
			// anda para o primeiro byte da entrada
			this.nextChar = inputStream.read();

		} catch (IOException e) {
			throw new TokenException(e.getMessage());
		}
	}

	/**
	 * Read the next byte from the inputstream to be converted as a char later
	 * to our analysis.
	 *
	 * @return the int which contains the byte of the char
	 * @throws TokenException
	 *             the token exception if some IOException is caught.
	 */
	private int readByte() throws TokenException {
		index++;
		int theByte = -1;

		try {
			theByte = inputStream.read();
		} catch (IOException e) {
			throw new TokenException(e.getMessage());
		}

		return theByte;
	}

	/**
	 * This method find the token to the correspondent char. It uses the concept
	 * of State Machines to construct Regular Expressions
	 *
	 * @return The object Token, which corresponds as the token of the given
	 *         char
	 * @throws TokenException
	 *             the token exception with its respective error message
	 */
	public Token nextToken() throws TokenException {
		TokenType tipo = null;
		String lexema = "";

		// Testa se chegou ao fim do arquivo
		if (this.nextChar == -1) {
			return new Token(TokenType.EOF);
		}

		while (this.nextChar == ' ' || this.nextChar == '\n'
				|| this.nextChar == '\t' || this.nextChar == '\r') {
			this.nextChar = this.readByte();
			if (this.nextChar == '\n') {
				line++;
				index = 0;
			}

			tipo = null;
			lexema = "";
		}

		// Analisa se tem um n�mero, se sim atribui o tipo do token como numero
		// e passa
		// para o pr�ximo caractere.
		if (nextChar >= '0' && nextChar <= '9') {
			tipo = TokenType.NUMBER;
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Enquanto o pr�ximo caractere for um n�mero ou uma letra.
			while (nextChar >= '0' && nextChar <= '9' || nextChar >= 'A'
					&& nextChar <= 'Z' || nextChar >= 'a' && nextChar <= 'z') {
				lexema += (char) nextChar;

				// Se achar uma letra atribui o tipo do token como
				// identificador e passa para o pr�ximo caractere.
				if (nextChar >= 'A' && nextChar <= 'Z' || nextChar >= 'a'
						&& nextChar <= 'z')
					tipo = TokenType.IDENTIFIER;
				this.nextChar = this.readByte();
			}

			// Se encontrar um ponto depois de um n�mero atribui o tipo do token
			// como float e passa para o pr�ximo caractere.
			if (nextChar == '.') {

				// Se o tipo j� for um identificador (Se na itera��o anterior
				// ter achado um caractere) joga uma exce��o para n�o permitir
				// identificadores com '.'.
				if (tipo == TokenType.IDENTIFIER) {
					throw new TokenException((char) nextChar, line, index,
							TokenException.IDENTIFIER_DOT);
				}

				tipo = TokenType.FLOAT;
				lexema += (char) nextChar;
				this.nextChar = this.readByte();

				// Procura os pr�ximos n�mero do float.
				while (nextChar >= '0' && nextChar <= '9') {
					lexema += (char) nextChar;
					this.nextChar = this.readByte();
				}

				// Se achar uma letra ou outro ponto joga uma exce��o para n�o
				// permitir letras e mais de um ponto no float.
				if (nextChar >= 'A' && nextChar <= 'Z' || nextChar >= 'a'
						&& nextChar <= 'z' || nextChar == '.'
						|| lexema.endsWith(".")) {
					throw new TokenException((char) nextChar, line, index,
							TokenException.EXPECTED_NUMBER);
				}
			}
		}

		// Analisa se tem uma letra, se sim atribui o tipo do token como
		// identificador e passa para o pr�ximo caractere.
		else if ((nextChar >= 'A' && nextChar <= 'Z')
				|| (nextChar >= 'a' && nextChar <= 'z')) {
			tipo = TokenType.IDENTIFIER;
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Procura pr�ximas letras ou n�meros do identificador.
			while ((nextChar >= '0' && nextChar <= '9')
					|| (nextChar >= 'A' && nextChar <= 'Z')
					|| (nextChar >= 'a' && nextChar <= 'z')) {
				lexema += (char) nextChar;
				this.nextChar = this.readByte();
			}

			// Procura se o lexema � alguma palavra reservada, caso n�o
			// seja, atribui novamente o tipo do token como identificador.
			TokenType novoTipo = (TokenType) RES_WORDS.get(lexema);
			if (novoTipo != null)
				tipo = novoTipo;
		}

		// Analisa o operador l�gico '>' e passa para o pr�ximo caractere.
		else if (nextChar == '>') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Se o operador l�gico vier com um '=' ap�s atribui o token de
			// "maior ou igual" e passa para o pr�ximo caractere, caso n�o,
			// atribui apenas o token de "maior que".
			if (nextChar == '=') {
				lexema += (char) nextChar;
				tipo = TokenType.REL_GREATER_OR_EQUALS;
				this.nextChar = this.readByte();
			} else
				tipo = TokenType.REL_GREATER_THAN;
		}

		// Analisa o operador l�gico '<' e passa para o pr�ximo caractere.
		else if (nextChar == '<') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Se o operador l�gico vier com um '=' ap�s atribui o token de
			// "menor ou igual" e passa para o pr�ximo caractere, caso n�o,
			// atribui apenas o token de "menor que".
			if (nextChar == '=') {
				lexema += (char) nextChar;
				tipo = TokenType.REL_LESS_OR_EQUALS;
				this.nextChar = this.readByte();
			} else
				tipo = TokenType.REL_LESS_THAN;
		}

		// Analisa o operador l�gico '=' e passa para o pr�ximo caractere.
		else if (nextChar == '=') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Se o operador l�gico vier com um '=' ap�s atribui o token de
			// "igual" e passa para o pr�ximo caractere, caso n�o,
			// atribui apenas o token de atribui��o.
			if (nextChar == '=') {
				lexema += (char) nextChar;
				tipo = TokenType.REL_EQUALS;
				this.nextChar = this.readByte();
			} else
				tipo = TokenType.OP_ASSIGNMENT;
		}

		// Analisa o operador l�gico '!' e passa para o pr�ximo caractere.
		else if (nextChar == '!') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Se o operador l�gico vier com um '=' ap�s atribui o token de
			// diferente e passa para o pr�ximo caractere, caso n�o,
			// atribui apenas o token de nega��o.
			if (nextChar == '=') {
				tipo = TokenType.REL_DIFFERENT;
				lexema += (char) nextChar;
				this.nextChar = this.readByte();
			} else
				tipo = TokenType.OP_NEGATION;
		}

		// Analisa o operador matem�tico '+', atribui o tipo do token como
		// soma e passa para o pr�ximo caractere.
		else if (nextChar == '+') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.OP_SUM;
		}

		// Analisa o operador matem�tico '-', atribui o tipo do token como
		// subtra��o e passa para o pr�ximo caractere.
		else if (nextChar == '-') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.OP_SUBTRACTION;
		}

		// Analisa o operador matem�tico '*', atribui o tipo do token como
		// multiplica��o e passa para o pr�ximo caractere.
		else if (nextChar == '*') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.OP_MULTIPLICATION;
		}

		// Analisa o operador matem�tico '/', atribui o tipo do token como
		// divis�o e passa para o pr�ximo caractere.
		else if (nextChar == '/') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.OP_DIVISION;

			// Caso ocorra outro '/' ignora todo o c�digo at� a pr�xima linha
			if (nextChar == '/') {
				while (nextChar != '\n') {
					lexema += (char) nextChar;
					nextChar = readByte();
				}
				line++;
				index = 0;
				Token nextToken = nextToken();
				tipo = nextToken.getTokenType();
				lexema = nextToken.getLexeme();
			}

			// Caso ocorra um '*' ignora todo o c�digo at� a pr�xima
			// ocorr�ncia de */
			else if (nextChar == '*') {
				while (!lexema.endsWith("*/")) {
					lexema += (char) nextChar;
					if (nextChar == '\n') {
						line++;
						index = 0;
					}
					nextChar = readByte();
					if (nextChar == -1) {
						throw new TokenException(TokenException.COMMENT_END);
					}
				}
				Token nextToken = nextToken();
				tipo = nextToken.getTokenType();
				lexema = nextToken.getLexeme();
			}
		}

		// Analisa o operador matem�tico '/', atribui o tipo do token como
		// porcentagem e passa para o pr�ximo caractere.
		else if (nextChar == '%') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.OP_PERCENTAGE;
		}

		// Analisa o operador l�gico '&' e passa para o pr�ximo caractere.
		else if (nextChar == '&') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Caso encontre outro '&' atribui o tipo do token como "And", caso
			// n�o, lan�a uma exce��o.
			if (nextChar == '&') {
				lexema += (char) nextChar;
				tipo = TokenType.OP_AND;
				this.nextChar = this.readByte();
			} else
				throw new TokenException('&', line, index,
						TokenException.EXPECTED_AND);

		} else if (nextChar == '|') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Caso encontre outro '|' atribui o tipo do token como "Or", caso
			// n�o, lan�a uma exce��o.
			if (nextChar == '|') {
				lexema += (char) nextChar;
				tipo = TokenType.OP_OR;
				this.nextChar = this.readByte();
			} else
				throw new TokenException('|', line, index,
						TokenException.EXPECTED_OR);
		}

		// Analisa o caractere especial '(', atribui seu tipo de token e
		// passa para o pr�ximo caractere.
		else if (nextChar == '(') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.SP_PARENTHESIS_LEFT;
		}

		// Analisa o caractere especial ')', atribui seu tipo de token e
		// passa para o pr�ximo caractere.
		else if (nextChar == ')') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.SP_PARENTHESIS_RIGHT;
		}

		// Analisa o caractere especial ',', atribui seu tipo de token e
		// passa para o pr�ximo caractere.
		else if (nextChar == ',') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.SP_COMMA;
		}

		// Analisa o caractere especial ';', atribui seu tipo de token e
		// passa para o pr�ximo caractere.
		else if (nextChar == ';') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.SP_SEMICOLON;
		}

		// Analisa o caractere especial ':', atribui seu tipo de token e
		// passa para o pr�ximo caractere.
		else if (nextChar == ':') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.SP_COLON;
		}

		// Analisa o caractere especial '{', atribui seu tipo de token e
		// passa para o pr�ximo caractere.
		else if (nextChar == '{') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.SP_CURLY_BRACKET_LEFT;
		}

		// Analisa o caractere especial '}', atribui seu tipo de token e
		// passa para o pr�ximo caractere.
		else if (nextChar == '}') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();
			tipo = TokenType.SP_CURLY_BRACKET_RIGHT;
		}

		// Analisa o caractere literal "'", passa para o pr�ximo caractere.
		else if (nextChar == '\'') {
			lexema += (char) nextChar;
			this.nextChar = this.readByte();

			// Caso o pr�ximo caractere seja uma letra ou um n�mero, adiciona ao
			// lexema
			if ((nextChar >= '0' && nextChar <= '9')
					|| (nextChar >= 'A' && nextChar <= 'Z')
					|| (nextChar >= 'a' && nextChar <= 'z') || nextChar == ' ') {
				lexema += (char) nextChar;
				this.nextChar = this.readByte();

				// Caso termine com "'", significa que est� no padr�o de
				// caractere e atribui o tipo de token do mesmo e carrega o
				// pr�ximo caractere, caso n�o siga o padr�o lan�a uma exce��o.
				if (nextChar == '\'') {
					lexema += (char) nextChar;
					tipo = TokenType.CHAR;
					this.nextChar = this.readByte();
				} else {
					throw new TokenException((char) nextChar, line, index,
							TokenException.EXPECTED_APOSTROPHE);
				}
			}

			// Caso o pr�ximo caractere seja o caractere '\'.
			else if (nextChar == '\\') {
				lexema += (char) nextChar;
				this.nextChar = this.readByte();

				// Analisa se ap�s o caractere vem um comando
				if (nextChar == 'n' || nextChar == 't' || nextChar == 'r'
						|| nextChar == '\\') {
					lexema += (char) nextChar;
					this.nextChar = this.readByte();

					// Verifica se o caractere terminou no padr�o, caso sim
					// atribui o tipo do token com o mesmo e passa para o
					// pr�ximo caractere, caso n�o, lan�a uma exce��o.
					if (nextChar == '\'') {
						lexema += (char) nextChar;
						tipo = TokenType.CHAR;
						this.nextChar = this.readByte();
					} else {
						throw new TokenException((char) nextChar, line, index,
								TokenException.EXPECTED_APOSTROPHE);
					}
				}

				// Caso ap�s o '/' n�o venha nenhum caractere de comando
				// lan�a uma exce��o
				else {
					throw new TokenException((char) nextChar, line, index,
							TokenException.EXPECTED_COMMAND);
				}
			}
		}

		// Caso o caractere atual n�o encontre nenhuma combina��o nas
		// verifica��es acima lan�a uma exce��o de Token n�o reconhecido
		else {
			throw new TokenException((char) nextChar, line, index);
		}

		return new Token(tipo, lexema);
	}

	/**
	 * Gets the line.
	 *
	 * @return the line
	 */
	public int getLine() {
		return line;
	}

	/**
	 * Gets the index.
	 *
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}
}
