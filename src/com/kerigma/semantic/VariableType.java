package com.kerigma.semantic;

public enum VariableType {
	INT, FLOAT, CHAR, BOOLEAN, VOID, NEGATION, NEGATIVE
}
