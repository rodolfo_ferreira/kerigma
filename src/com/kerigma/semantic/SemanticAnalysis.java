/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package com.kerigma.semantic;

import java.util.ArrayList;
import java.util.Hashtable;

import com.kerigma.lexer.Token;
import com.kerigma.lexer.TokenType;
import com.kerigma.parser.FunctionType;

import dolphs.util.HashtableD;
import dolphs.util.tree.Leaf;
import dolphs.util.tree.Node;
import dolphs.util.tree.Tree;
import dolphs.util.tree.Visitor;

// TODO: Auto-generated Javadoc
/**
 * The Class SemanticAnalysis.
 */
@SuppressWarnings("unchecked")
public class SemanticAnalysis implements Visitor {

	/** The variable table stack. */
	private ArrayList<Hashtable<String, TokenType>> variableTableStack = new ArrayList<Hashtable<String, TokenType>>();

	/** The function table stack. */
	private ArrayList<HashtableD<String, ArrayList<TokenType>, ArrayList<Object>>> functionTableStack = new ArrayList<HashtableD<String, ArrayList<TokenType>, ArrayList<Object>>>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see dolphs.util.tree.Visitor#visit(dolphs.util.tree.Node)
	 */
	@Override
	public Object visit(Node<?> node) {
		Object object = node.get();
		if (object instanceof FunctionType) {
			FunctionType func = (FunctionType) object;
			switch (func) {
			case PROGRAMA:
				analyseProgramNode(node);
				break;
			case BLOCO:
				analyseBlock(node);
				break;
			case DECL_FUNC:
				anaylseDeclFunc(node);
				break;
			case DECL_VARIAVEL:
				analyseDeclVariavel(node);
				break;
			case DECL_GLOBAL:
				analyseDeclGlobal(node);
				break;
			case ASSINATURA:
				analyseSignature(node);
				break;
			case ATRIBUICAO:
				analyseAssignment(node);
				break;
			case CHAMADA_FUNC:
				analyseFuncCall(node);
				break;
			case COMANDO:
				analyseCommand(node);
				break;
			case DECISAO:
				analyseDecision(node);
				break;
			case ESCRITA:
				analyseWrite(node);
				break;
			case RETORNO:
				analyseReturn(node);
				break;
			case TIPO:
				return analyseType(node);
			case ITERACAO:
				analyseIteraction(node);
				break;
			case LISTA_COMANDOS:
				analyseCommandList(node);
				break;
			case LISTA_EXPRS:
				return analyseExprList(node);
			case LISTA_IDENTS:
				return getListIdents(node);
			case LISTA_TIPOS:
				return getTypeList(node);
			case PARAM_FORMAIS:
				return analyseFormalParams(node);
			case EXPRESSAO:
				return analyseExpr(node);
			case EXPRESSAO_A:
				return analyseExprA(node);
			case EXPRESSAO_A_COMP:
				return analyseExprAComp(node);
			case EXPRESSAO_B:
				return analyseExprB(node);
			case EXPRESSAO_C:
				return analyseExprC(node);
			case EXPRESSAO_D:
				return analyseExprD(node);
			case EXPRESSAO_BASICA:
				return analyseBasicExpr(node);
			default:
				break;

			}
		}
		return true;
	}

	/**
	 * Analyse return.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseReturn(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		for (int i = 0; i < length; i++) {
			Tree children = childrens.get(i);
			if (children instanceof Node<?>)
				visit(children);
		}

	}

	/**
	 * Analyse iteraction.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseIteraction(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		for (int i = 0; i < length; i++) {
			Tree children = childrens.get(i);
			Object object = visit(children);
			if (object instanceof SemanticExpression) {
				SemanticExpression semanticExpr = (SemanticExpression) object;
				if (semanticExpr.getType() != VariableType.BOOLEAN) {
					throw new SemanticAnalysisException(
							"The Expression returns non Boolean");
				}
			}
		}
	}

	/**
	 * Analyse expr list.
	 *
	 * @param node
	 *            the node
	 * @return the object
	 */
	private Object analyseExprList(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		ArrayList<Bind> result = new ArrayList<Bind>();
		for (int i = 0; i < length; i++) {
			if (i % 2 == 0) {
				SemanticExpression semanticExpr = (SemanticExpression) visit(childrens
						.get(i));
				result.addAll(semanticExpr.getBinds());
			}
		}
		return result;
	}

	/**
	 * Analyse type.
	 *
	 * @param node
	 *            the node
	 * @return the object
	 */
	private Object analyseType(Node<?> node) {
		return node.getChildrens().get(0);
	}

	/**
	 * Analyse formal params.
	 *
	 * @param node
	 *            the node
	 * @return the object
	 */
	private Object analyseFormalParams(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		Hashtable<String, TokenType> hashtable = variableTableStack
				.get(variableTableStack.size() - 1);
		ArrayList<TokenType> tokenTypes = new ArrayList<TokenType>();
		String key = null;
		TokenType value = null;
		for (int i = 0; i < length; i++) {
			Tree childrenTree = childrens.get(i);
			if (i % 3 == 0) {
				key = ((Token) ((Leaf<?>) childrenTree).get()).getLexeme();
			} else if (i % 3 == 1) {
				value = ((Token) ((Leaf<?>) visit(childrenTree)).get())
						.getTokenType();
				tokenTypes.add(value);
			}

			if (key != null && value != null) {
				hashtable.put(key, value);
				key = null;
				value = null;
			}

		}
		return tokenTypes;
	}

	/**
	 * Analyse expr d.
	 *
	 * @param node
	 *            the node
	 * @return the semantic expression
	 */
	private SemanticExpression analyseExprD(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		SemanticExpression result = null;
		int length = childrens.size();
		if (length == 3) {
			result = new SemanticExpression();
			SemanticExpression expr1 = (SemanticExpression) visit(childrens
					.get(0));
			Token token = (Token) ((Tree) visit(childrens.get(1))).get();
			SemanticExpression expr2 = (SemanticExpression) visit(childrens
					.get(2));
			result.bind(expr1, token.getTokenType(), expr2);
		} else {
			result = (SemanticExpression) visit(childrens.get(0));
		}
		return result;
	}

	/**
	 * Analyse expr c.
	 *
	 * @param node
	 *            the node
	 * @return the semantic expression
	 */
	private SemanticExpression analyseExprC(Node<?> node) {
		SemanticExpression result = null;
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		if (length == 3) {
			result = new SemanticExpression();
			SemanticExpression expr1 = (SemanticExpression) visit(childrens
					.get(0));
			Token token = (Token) ((Tree) visit(childrens.get(1))).get();
			SemanticExpression expr2 = (SemanticExpression) visit(childrens
					.get(2));
			result.bind(expr1, token.getTokenType(), expr2);
		} else {
			result = (SemanticExpression) visit(childrens.get(0));
		}
		return result;
	}

	/**
	 * Analyse expr b.
	 *
	 * @param node
	 *            the node
	 * @return the semantic expression
	 */
	private SemanticExpression analyseExprB(Node<?> node) {
		SemanticExpression result = new SemanticExpression();
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		if (length == 3) {
			result = new SemanticExpression();
			SemanticExpression expr1 = (SemanticExpression) visit(childrens
					.get(0));
			Token token = (Token) (((Tree) visit(childrens.get(1))).get());
			SemanticExpression expr2 = (SemanticExpression) visit(childrens
					.get(2));
			result.bind(expr1, token.getTokenType(), expr2);
		} else {
			SemanticExpression semantic = (SemanticExpression) visit(childrens
					.get(0));
			result = semantic;
		}
		return result;
	}

	/**
	 * Analyse expr a comp.
	 *
	 * @param node
	 *            the node
	 * @return the semantic expression
	 */
	private SemanticExpression analyseExprAComp(Node<?> node) {
		SemanticExpression result = new SemanticExpression();
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		if (length == 3) {
			result = new SemanticExpression();
			SemanticExpression expr1 = (SemanticExpression) visit(childrens
					.get(0));
			Token token = (Token) visit(childrens.get(1));
			SemanticExpression expr2 = (SemanticExpression) visit(childrens
					.get(2));
			result.bind(expr1, token.getTokenType(), expr2);
		} else {
			result = (SemanticExpression) visit(childrens.get(0));
		}
		return result;
	}

	/**
	 * Analyse expr a.
	 *
	 * @param node
	 *            the node
	 * @return the semantic expression
	 */
	private SemanticExpression analyseExprA(Node<?> node) {
		SemanticExpression result = new SemanticExpression();
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		if (length == 3) {
			result = new SemanticExpression();
			SemanticExpression expr1 = (SemanticExpression) visit(childrens
					.get(0));
			Token token = (Token) ((Tree) visit(childrens.get(1))).get();
			SemanticExpression expr2 = (SemanticExpression) visit(childrens
					.get(2));
			result.bind(expr1, token.getTokenType(), expr2);
		} else {
			result = (SemanticExpression) visit(childrens.get(0));
		}
		return result;
	}

	/**
	 * Analyse expr.
	 *
	 * @param node
	 *            the node
	 * @return the semantic expression
	 */
	private SemanticExpression analyseExpr(Node<?> node) {
		SemanticExpression result = new SemanticExpression();
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		SemanticExpression semanticExpr = null;
		for (int i = 0; i < length; i++) {
			Tree childrenTree = childrens.get(i);
			semanticExpr = (SemanticExpression) visit(childrenTree);
			result = semanticExpr;

		}

		return result;
	}

	/**
	 * Analyse write.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseWrite(Node<?> node) {
		visit(node.getChildrens().get(2));

	}

	/**
	 * Analyse basic expr.
	 *
	 * @param node
	 *            the node
	 * @return the token
	 */
	private SemanticExpression analyseBasicExpr(Node<?> node) {
		int length = node.getChildrens().size();
		Bind bind = null;
		SemanticExpression result = new SemanticExpression();
		if (length == 4) {
			Leaf<?> leaf = (Leaf<?>) node.getChildrens().get(0);
			Token token = (Token) leaf.get();
			bind = getReturnFromFunctionTable(token);
			if (bind == null)
				throw new SemanticAnalysisException(token.getLexeme()
						+ " cannot be resolved to a function call");
			result.addBind(bind);
		} else {
			for (int i = 0; i < length; i++) {
				Tree tree = node.getChildrens().get(i);
				if (tree instanceof Leaf<?>) {
					Leaf<?> leaf = (Leaf<?>) tree;
					Token token = (Token) leaf.get();
					if (token.getTokenType() == TokenType.IDENTIFIER) {
						bind = getFromVariableTable(token);
						if (bind == null)
							throw new SemanticAnalysisException(
									token.getLexeme()
											+ " cannot be resolved to a variable");

					} else if (token.getTokenType() == TokenType.OP_NEGATION
							|| token.getTokenType() == TokenType.OP_SUBTRACTION) {
						bind = new Bind(token);
					} else {
						try {
							bind = new Bind(token);
						} catch (Exception e) {

						}
					}
					if (bind != null)
						result.addBind(bind);
				} else {
					result.addBinds((SemanticExpression) visit(tree));
				}
			}
		}

		return result;
	}

	private Bind getReturnFromFunctionTable(Token token) {
		Bind result = null;
		int stackLength = functionTableStack.size();
		for (int j = stackLength - 1; j >= 0; j--) {
			ArrayList<Object> obj = functionTableStack.get(j).getC(
					token.getLexeme());
			if (obj != null) {
				Token localToken = (Token) obj.get(0);
				if (localToken != null) {
					result = new Bind(token.getLexeme(),
							localToken.getTokenType());
					break;
				}
			}
		}
		return result;
	}

	private int getReturnNumberFromFunctionTable(Bind bind) {
		int result = 0;
		int stackLength = functionTableStack.size();
		for (int j = stackLength - 1; j >= 0; j--) {
			Object object = functionTableStack.get(j).getC(bind.getVariable());
			if (object != null) {
				int number = functionTableStack.get(j).getC(bind.getVariable())
						.size();
				if (number > 0) {
					result = number;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Analyse assignment.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseAssignment(Node<?> node) {
		ArrayList<Token> idents = (ArrayList<Token>) visit(node.getChildrens()
				.get(0));
		int identLength = idents.size();
		for (int i = 0; i < identLength; i++) {
			Token token = idents.get(i);
			Bind bind = getFromVariableTable(token);
			if (bind == null) {
				throw new SemanticAnalysisException(token.getLexeme()
						+ " cannot be resolved to a variable");
			}
		}

		SemanticExpression semanticExpression = (SemanticExpression) visit(node
				.getChildrens().get(2));
		if (identLength == 1) {
			Token token = idents.get(0);
			Bind bind;
			if (token.getTokenType() == TokenType.IDENTIFIER) {
				bind = getFromVariableTable(token);
			} else {
				bind = new Bind(token);
			}
			Bind thisBind = (Bind) semanticExpression.getBinds().getLast();
			int returns = getReturnNumberFromFunctionTable(thisBind);
			if (returns > 1) {
				throw new SemanticAnalysisException(
						"Function call has more than one return");
			} else if (bind.getType() != semanticExpression.getType()) {
				throw new SemanticAnalysisException(
						"Left part of assignment is a \"" + bind.getType()
								+ "\" and the right is a \""
								+ semanticExpression.getType() + "\"");
			}
		}

	}

	/**
	 * Analyse func call.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseFuncCall(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		Tree tree = childrens.get(0);
		Token token = (Token) tree.get();
		ArrayList<TokenType> params = null;
		ArrayList<Object> returns = null;
		int tableStackLength = functionTableStack.size();
		for (int i = tableStackLength - 1; i >= 0; i--) {
			params = functionTableStack.get(i).getB(token.getLexeme());
			returns = functionTableStack.get(i).getC(token.getLexeme());
			if (params != null || returns != null)
				break;
		}
		if (params == null && returns == null) {
			String message = "The method " + token.getLexeme() + "(";
			ArrayList<Token> idents = (ArrayList<Token>) visit(childrens.get(2));
			int length = idents.size();
			for (int i = 0; i < length; i++) {
				message += getFromVariableTable(idents.get(i));
				if (i < length - 1)
					message += ", ";

			}
			message += ")" + " is undefined";
			throw new SemanticAnalysisException(message);
		}
		ArrayList<Bind> paramsIdent = (ArrayList<Bind>) visit(childrens.get(2));
		ArrayList<Bind> paramsType = new ArrayList<Bind>();
		for (int i = 0; i < paramsIdent.size(); i++) {
			paramsType.add(paramsIdent.get(i));
		}
		HashtableD<String, ArrayList<TokenType>, ArrayList<Object>> table = functionTableStack
				.get(functionTableStack.size() - 1);
		ArrayList<TokenType> toCompare = table.getB(token.getLexeme());
		boolean paramsOk = false;
		if ((paramsType != null && toCompare != null)
				&& paramsType.size() == toCompare.size()) {
			boolean match = true;
			for (int i = 0; i < paramsType.size(); i++) {
				VariableType callParamType = paramsType.get(i).getType();
				VariableType funcParamType = (new Bind(null, toCompare.get(i)))
						.getType();
				if (callParamType != funcParamType) {
					match = false;
					break;
				}
			}
			if (match)
				paramsOk = true;
		}

		if (!paramsOk) {
			String message = "The method " + token.getLexeme() + "(";
			ArrayList<Bind> idents = (ArrayList<Bind>) visit(childrens.get(2));
			int length = idents.size();
			for (int i = 0; i < length; i++) {
				message += idents.get(i).getType();
				if (i < length - 1)
					message += ", ";

			}
			message += ")" + " is undefined";
			throw new SemanticAnalysisException(message);
		}
	}

	/**
	 * Gets the given token from variable hash table.
	 *
	 * @param token
	 *            the token
	 * @return the from table
	 */
	private Bind getFromVariableTable(Token token) {
		Bind result = null;
		TokenType tokenType = null;
		int stackLength = variableTableStack.size();
		for (int j = stackLength - 1; j >= 0; j--) {
			tokenType = variableTableStack.get(j).get(token.getLexeme());
			if (tokenType != null) {
				result = new Bind(token.getLexeme(), tokenType);
				break;
			}
		}
		return result;
	}

	/**
	 * Analyse decision.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseDecision(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		for (int i = 0; i < length; i++) {
			Tree childrenTree = childrens.get(i);
			Object object = visit(childrenTree);
			if (i == 2 && object instanceof SemanticExpression) {
				SemanticExpression semanticExpr = (SemanticExpression) object;
				if (semanticExpr.getType() != VariableType.BOOLEAN) {
					throw new SemanticAnalysisException(
							"The Expression returns non Boolean");
				}
			}
		}

	}

	/**
	 * Analyse command.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseCommand(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		for (int i = 0; i < length; i++) {
			visit(childrens.get(i));
		}

	}

	/**
	 * Analyse command list.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseCommandList(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		for (int i = 0; i < length; i++) {
			visit(childrens.get(i));
		}

	}

	/**
	 * Analyse block.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseBlock(Node<?> node) {
		Hashtable<String, TokenType> newTable = new Hashtable<String, TokenType>();
		variableTableStack.add(newTable);
		visit(node.getChildrens().get(1));
		variableTableStack.remove(variableTableStack.size() - 1);
	}

	/**
	 * Analyse signature.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseSignature(Node<?> node) {
		ArrayList<Tree> childrens = node.getChildrens();
		int length = childrens.size();
		for (int i = 0; i < length; i++) {
			visit(childrens.get(i));
		}

		int stackLength = functionTableStack.size();
		String lexeme = ((Token) ((Tree) visit(childrens.get(1))).get())
				.getLexeme();
		for (int j = 0; j < stackLength; j++) {
			if (functionTableStack.get(j).getB(lexeme) != null)
				throw new SemanticAnalysisException(
						"Duplicated Local Variable " + lexeme);
		}
		HashtableD<String, ArrayList<TokenType>, ArrayList<Object>> lastTable = functionTableStack
				.get(stackLength - 1);
		ArrayList<TokenType> params = (ArrayList<TokenType>) visit(node
				.getChildrens().get(3));
		Object object = visit(node.getChildrens().get(6));
		ArrayList<Object> returns = null;
		if (object instanceof ArrayList)
			returns = (ArrayList<Object>) object;
		else if (object instanceof Leaf) {
			returns = new ArrayList<Object>();
			returns.add(visit((Leaf<?>) object));
		}
		lastTable.put(lexeme, params, returns);

	}

	/**
	 * Anaylse decl func.
	 *
	 * @param node
	 *            the node
	 */
	private void anaylseDeclFunc(Node<?> node) {
		int length = node.getChildrens().size();
		Hashtable<String, TokenType> variable = new Hashtable<String, TokenType>();
		variableTableStack.add(variable);
		for (int i = 0; i < length; i++) {
			visit((Node<?>) node.getChildrens().get(i));
		}
		variableTableStack.remove(variable);

	}

	/**
	 * Analyse decl global.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseDeclGlobal(Node<?> node) {
		Node<?> childrenNode = (Node<?>) node.getChildrens().get(0);
		visit(childrenNode);

	}

	/**
	 * Analyse decl variavel.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseDeclVariavel(Node<?> node) {
		int length = node.getChildrens().size();
		ArrayList<Token> identList = null;
		ArrayList<Token> typeList = null;
		for (int i = 0; i < length; i++) {
			Tree tree = node.getChildrens().get(i);
			if (tree instanceof Node<?>) {
				Node<?> thisNode = (Node<?>) tree;
				FunctionType func = (FunctionType) thisNode.get();
				if (func.equals(FunctionType.LISTA_IDENTS)) {
					identList = (ArrayList<Token>) visit(thisNode);
				} else if (func.equals(FunctionType.LISTA_TIPOS)) {
					typeList = (ArrayList<Token>) visit(thisNode);
				}
			}
		}

		length = identList.size();
		if (length == typeList.size()) {
			Hashtable<String, TokenType> table = variableTableStack
					.get(variableTableStack.size() - 1);
			for (int i = 0; i < length; i++) {
				int stackLength = variableTableStack.size();
				String lexeme = identList.get(i).getLexeme();
				for (int j = 0; j < stackLength; j++) {
					if (variableTableStack.get(j).get(lexeme) != null)
						throw new SemanticAnalysisException(
								"Duplicated Local Variable "
										+ identList.get(i).getLexeme());
				}
				table.put(lexeme, typeList.get(i).getTokenType());
			}
		} else {
			throw new SemanticAnalysisException(
					"Invalid variable declarator, the left and the right part of the declarator must have the same number of tokens");
		}

	}

	/**
	 * Gets the type list.
	 *
	 * @param node
	 *            the node
	 * @return the type list
	 */
	private ArrayList<Token> getTypeList(Node<?> node) {
		ArrayList<Token> result = new ArrayList<Token>();
		int length = node.getChildrens().size();
		for (int i = 0; i < length; i++) {
			Tree tree = node.getChildrens().get(i);
			if (tree instanceof Node) {
				Node<?> childrenNode = (Node<?>) tree;
				FunctionType childFunctionType = (FunctionType) childrenNode
						.get();
				if (childFunctionType.equals(FunctionType.TIPO)) {
					Leaf<?> tokenNode = (Leaf<?>) childrenNode.getChildrens()
							.get(0);
					Token token = (Token) tokenNode.get();
					result.add(token);
				}
			}
		}
		return result;

	}

	/**
	 * Gets the list idents.
	 *
	 * @param thisNode
	 *            the this node
	 * @return the list idents
	 */
	private ArrayList<Token> getListIdents(Node<?> thisNode) {
		ArrayList<Token> result = new ArrayList<Token>();
		int length = thisNode.getChildrens().size();
		for (int i = 0; i < length; i++) {
			Leaf<?> leaf = (Leaf<?>) thisNode.getChildrens().get(i);
			Token token = (Token) leaf.get();
			if (token.getTokenType().equals(TokenType.IDENTIFIER))
				result.add(token);
		}
		return result;
	}

	/**
	 * Analyse program node.
	 *
	 * @param node
	 *            the node
	 */
	private void analyseProgramNode(Node<?> node) {
		Hashtable<String, TokenType> hashtable = new Hashtable<String, TokenType>();
		variableTableStack.add(hashtable);
		HashtableD<String, ArrayList<TokenType>, ArrayList<Object>> funcTable = new HashtableD<String, ArrayList<TokenType>, ArrayList<Object>>();
		functionTableStack.add(funcTable);
		int length = node.getChildrens().size();
		for (int i = 0; i < length; i++) {
			Tree tree = node.getChildrens().get(i);
			if (tree instanceof Node<?>)
				visit((Node<?>) tree);
			else
				visit((Leaf<?>) tree);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dolphs.util.tree.Visitor#visit(dolphs.util.tree.Leaf)
	 */
	@Override
	public Object visit(Leaf<?> leaf) {
		Token tokenType = (Token) leaf.get();
		return tokenType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dolphs.util.tree.Visitor#visit(dolphs.util.tree.Tree)
	 */
	@Override
	public Object visit(Tree tree) {
		if (tree instanceof Node<?>) {
			Node<?> node = (Node<?>) tree;
			return visit(node);
		} else if (tree instanceof Leaf<?>) {
			Leaf<?> leaf = (Leaf<?>) tree;
			return leaf;
		} else {
			throw new ClassCastException();
		}
	}

	/**
	 * Validate.
	 */
	public void validate() {
		if (variableTableStack.size() != 1)
			throw new SemanticAnalysisException(
					"You still have tables in your semantic analisis");

	}
}
