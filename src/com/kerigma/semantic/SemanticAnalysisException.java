package com.kerigma.semantic;

public class SemanticAnalysisException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2532269978626955470L;

	public SemanticAnalysisException(String message) {
		super(message);
	}
}
