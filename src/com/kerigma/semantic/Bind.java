package com.kerigma.semantic;

import com.kerigma.lexer.Token;
import com.kerigma.lexer.TokenType;

public class Bind {

	private String variable;

	private VariableType type;

	public Bind(String variable, TokenType type) {
		this.variable = variable;
		this.type = getType(type);
	}

	public Bind(Token token) {
		this.variable = token.getLexeme();
		this.type = getType(token.getTokenType());
	}

	private VariableType getType(TokenType type) {
		VariableType result;
		if (type == TokenType.RES_INT || type == TokenType.NUMBER)
			result = VariableType.INT;
		else if (type == TokenType.RES_FLOAT || type == TokenType.FLOAT)
			result = VariableType.FLOAT;
		else if (type == TokenType.RES_CHAR || type == TokenType.CHAR)
			result = VariableType.CHAR;
		else if (type == TokenType.RES_VOID) {
			result = VariableType.VOID;
		} else if (type == TokenType.OP_NEGATION) {
			result = VariableType.NEGATION;
		} else if (type == TokenType.OP_SUBTRACTION) {
			result = VariableType.NEGATIVE;
		} else
			throw new SemanticAnalysisException("The type " + type
					+ " has no match");
		return result;
	}

	public String getVariable() {
		return variable;
	}

	public VariableType getType() {
		return type;
	}

	@Override
	public String toString() {
		return "{" + getVariable() + "=" + getType() + "}";
	}
}
