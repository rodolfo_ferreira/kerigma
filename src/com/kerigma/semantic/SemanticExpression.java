package com.kerigma.semantic;

import com.kerigma.lexer.TokenType;

import dolphs.util.VectorD;

public class SemanticExpression {

	private VectorD<Bind> bind;

	private VariableType type;

	public SemanticExpression() {
		this.bind = new VectorD<Bind>();
	}

	// public SemanticExpression(Bind... binds) {
	// this.bind = new VectorD<Bind>();
	// addBinds(binds);
	// }
	//
	// public SemanticExpression(Bind bind) {
	// this.bind = new VectorD<Bind>();
	// addBind(bind);
	// }
	//
	// public SemanticExpression(List<? extends Bind> binds) {
	// this.bind = new VectorD<Bind>();
	// addBinds(binds);
	// }
	//
	// public void addBinds(Bind... binds) {
	// int length = binds.length;
	// for (int i = 0; i < length; i++) {
	// Bind bind = binds[i];
	// checkType(bind);
	// this.bind.add(bind);
	// }
	// }

	public void addBind(Bind bind) {
		if (this.type == null || this.type == VariableType.NEGATIVE
				|| this.type == VariableType.NEGATION)
			this.type = bind.getType();
		this.bind.add(bind);
	}

	// public void addBinds(List<? extends Bind> binds, TokenType operation) {
	// int length = binds.size();
	// for (int i = 0; i < length; i++) {
	// Bind bind = binds.get(i);
	// checkType(bind, operation);
	// this.bind.add(bind);
	// }
	// }
	//
	// private void checkType(Bind bind, TokenType operation) {
	// VariableType defaultType = this.getType();
	// VariableType bindType = bind.getType();
	// if (defaultType == null) {
	// this.type = bindType;
	// } else if (bindType != defaultType) {
	// if (bindType == VariableType.INT
	// && defaultType == VariableType.CHAR) {
	// this.type = VariableType.CHAR;
	// } else {
	// throw new SemanticAnalysisException(
	// "Can't perform operations between " + defaultType
	// + " and " + bindType);
	// }
	// }
	// if (bind.size() > 0) {
	// VariableType defaultVariableType = bind.get(0).getType();
	// for (int i = 1; i < length; i++) {
	// VariableType variableType = bind.get(i).getType();
	// if (variableType != defaultVariableType) {
	// if (defaultVariableType == VariableType.CHAR
	// && variableType == VariableType.INT) {
	// type = VariableType.CHAR;
	// } else {
	// throw new SemanticAnalysisException(
	// "Can't perform operations between "
	// + defaultVariableType + " and "
	// + variableType);
	// }
	// }
	// }
	// this.type = defaultVariableType;
	// }
	// }

	public void addBinds(SemanticExpression semanticExpression) {
		VectorD<Bind> binds = semanticExpression.getBinds();
		int length = binds.size();
		if (this.type == VariableType.NEGATION
				&& (semanticExpression.getType() == VariableType.BOOLEAN))
			this.type = semanticExpression.getType();
		else if (this.type == VariableType.NEGATIVE
				&& (semanticExpression.getType() == VariableType.INT || semanticExpression
						.getType() == VariableType.FLOAT)) {
			this.type = semanticExpression.getType();
		} else if (this.type == null) {
			this.type = semanticExpression.getType();
		} else if (this.type == VariableType.NEGATION
				|| this.type == VariableType.NEGATIVE) {
			throw new SemanticAnalysisException("The operator " + this.type
					+ " is undefined for the argument type(s) "
					+ semanticExpression.getType());
		}
		for (int i = 0; i < length; i++) {
			Bind bind = binds.get(i);
			addBind(bind);

		}
	}

	public VectorD<Bind> getBinds() {
		return bind;
	}

	public VariableType getType() {
		return type;
	}

	public void bind(SemanticExpression expr1, TokenType tokenType,
			SemanticExpression expr2) {
		if (tokenType == TokenType.OP_SUM
				|| tokenType == TokenType.OP_SUBTRACTION) {
			if (checkTypes(expr1, expr2)) {
				this.type = expr1.getType();
			} else if (checkSumSubTypes(expr1, expr2)) {
				this.type = expr1.getType();
			} else {
				throw new SemanticAnalysisException("Can't perform "
						+ tokenType + " between " + expr1.getType() + " and "
						+ expr2.getType());
			}
		} else if (tokenType == TokenType.OP_MULTIPLICATION
				|| tokenType == TokenType.OP_DIVISION) {
			if (checkTypes(expr1, expr2)) {
				this.type = expr1.getType();
			} else {
				throw new SemanticAnalysisException("Can't perform "
						+ tokenType + " between " + expr1.getType() + " and "
						+ expr2.getType());
			}
		} else if (tokenType == TokenType.REL_GREATER_OR_EQUALS
				|| tokenType == TokenType.REL_GREATER_THAN
				|| tokenType == TokenType.REL_LESS_OR_EQUALS
				|| tokenType == TokenType.REL_LESS_THAN) {
			if (checkTypes(expr1, expr2)) {
				this.type = VariableType.BOOLEAN;
			} else {
				throw new SemanticAnalysisException("Can't perform "
						+ tokenType + " between " + expr1.getType() + " and "
						+ expr2.getType());
			}
		} else if (tokenType == TokenType.REL_EQUALS
				|| tokenType == TokenType.REL_DIFFERENT) {
			if (checkRelTypes(expr1, expr2)) {
				this.type = VariableType.BOOLEAN;
			} else {
				throw new SemanticAnalysisException("Can't perform "
						+ tokenType + " between " + expr1.getType() + " and "
						+ expr2.getType());
			}
		} else if (tokenType == TokenType.OP_AND
				|| tokenType == TokenType.OP_OR) {
			if (checkValTypes(expr1, expr2)) {
				this.type = VariableType.BOOLEAN;
			} else {
				throw new SemanticAnalysisException("Can't perform "
						+ tokenType + " between " + expr1.getType() + " and "
						+ expr2.getType());
			}
		}
		addBinds(expr1);
		addBinds(expr2);

	}

	private boolean checkValTypes(SemanticExpression expr1,
			SemanticExpression expr2) {
		return expr1.getType() == expr2.getType()
				&& expr1.getType() == VariableType.BOOLEAN;
	}

	private boolean checkSumSubTypes(SemanticExpression expr1,
			SemanticExpression expr2) {
		return (expr1.getType() == VariableType.CHAR)
				&& (expr2.getType() == VariableType.INT);
	}

	private boolean checkTypes(SemanticExpression expr1,
			SemanticExpression expr2) {
		return expr1.getType() == expr2.getType()
				&& (expr1.getType() == VariableType.FLOAT || expr1.getType() == VariableType.INT);
	}

	private boolean checkRelTypes(SemanticExpression expr1,
			SemanticExpression expr2) {
		return expr1.getType() == expr2.getType()
				&& (expr1.getType() == VariableType.FLOAT
						|| expr1.getType() == VariableType.INT
						|| expr1.getType() == VariableType.BOOLEAN || expr1
						.getType() == VariableType.CHAR);
	}

	@Override
	public String toString() {
		return type.toString();
	}
}
